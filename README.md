# Rust AWS Lambda Text Processing Project

This project includes two AWS Lambda functions written in Rust to handle text processing. The first function identifies whether the input text contains impolite words, returning a classification as "Impolite" or "Normal". The second function sanitizes the input text by replacing identified impolite words with asterisks.

## Prerequisites

Before you begin, ensure you have the following installed:

- Rust and Cargo (See [Rust Installation](https://www.rust-lang.org/tools/install))
- AWS CLI configured with your AWS account (See [AWS CLI Configuration](https://docs.aws.amazon.com/cli/latest/userguide/cli-chap-configure.html))
- `cargo-lambda` (Install via Cargo with `cargo install cargo-lambda`)

## Setup

Clone this repository to your local machine and navigate into the project directory:

```bash
git clone <URL-of-this-repository>
cd <project-directory>
```

## Project Structure
The project contains two main directories:

- `filter_txt/` - Contains the Lambda function for filtering impolite words with *.
- `warn_txt/` - Contains the Lambda function for giving user warning text.

Each directory has a `src/main.rs` file where the Lambda function logic is implemented.

## Building the Functions

To build the Lambda functions, use the following commands in their respective directories:

```bash
# For each function
cargo lambda build --release
```
- screenshot of build filter_txt
![build1](build2.png)
- screenshot of build warn_txt
![build2](build1.png)

## Testing the Functions locally

Test the functions locally by invoking them with test data:

using cargo lambda watch to start the local test environment
```bash
cargo lambda watch
```
Create an `event.json` in the `text_checker` directory:

```json
{
    "text": "fuck the world, this is a message with impolite words."
}

```

Invoke the function respectively:

```bash
cargo lambda invoke --data-file event.json --output response.json
```

**Results**
- screenshot of filter_txt
![filter](filter.png)


## Deploy on AWS Lambda
1. Create a role with policies `AWSLambda_FullAccess`, `AWSLambdaBasicExecutionRole`, `IAMFullAccess`.
2. Obtain the binary file by building the project:
    ```
    cargo lambda build --release
    ```
3. Make sure to set up the AWS configuration and deploy by:
    ```
    cargo lambda deploy --region <REGION> --iam-role <ROLE_ARN>
    ```
Then you can check your AWS Lambda function on AWS Lambda.

### Build Corresponding Step Functions
1. Create a new State Machine in AWS Step Functions with a proper template.
2. Implement the workflow on the website
    ```
3. Execute the state machine with input to test if the workflow can work correctly.


## Srceenshots
### Execution Input 
![input](input.png)

### Execution success
![success](success.png)

### Output of step1
![image](output1.png)

### Output of step2
![image](output2.png)

## Demo Video
The demo video shows the workflow of Step Functions and functionality of the Lambdas.
demo link: https://youtu.be/exyMSmlX6Ig 
