use lambda_runtime::{handler_fn, Context, Error};
use serde_json::{Value, json};

#[tokio::main]
async fn main() -> Result<(), Error> {
    lambda_runtime::run(handler_fn(handler)).await?;
    Ok(())
}

async fn handler(event: Value, _: Context) -> Result<Value, Error> {
    let text = event["sanitized_text"].as_str().unwrap_or("");

    let check_text = |text: &str| {
        let impolite_words = ["fuck", "shit", "damn", "bitch", "*"];
        impolite_words.iter().any(|&word| text.to_lowercase().contains(word))
    };

    let response = if check_text(text) {
        "Hey!! this is not polite"
    } else {
        "Hahaahah."
    };

    Ok(json!({ "result": response }))
}
