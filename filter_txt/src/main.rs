use lambda_runtime::{handler_fn, Context, Error};
use serde_json::{json, Value};

#[tokio::main]
async fn main() -> Result<(), Error> {
    lambda_runtime::run(handler_fn(handler)).await?;
    Ok(())
}

async fn handler(event: Value, _: Context) -> Result<Value, Error> {
    let text = event["text"].as_str().unwrap_or_default();

    let sanitized_text = sanitize_text(text);

    Ok(json!({ "sanitized_text": sanitized_text }))
}

fn sanitize_text(text: &str) -> String {
    let impolite_words = ["fuck", "shit", "damn"]; // List of words to sanitize
    let mut result_text = text.to_owned();

    for word in impolite_words.iter() {
        let replacement = "*".repeat(word.len());
        // Using replace here ensures all instances of the word are sanitized
        result_text = result_text.replace(word, &replacement);
    }

    result_text
}
